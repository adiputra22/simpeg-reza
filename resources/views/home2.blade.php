@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <form action="{{ url('/create') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" id="nama" name="nama" class="form-control">
                            <label>Nama perusahaan</label>
                            <select class="form-control" name="perusahaan">
                            @foreach($perusahaans as $perusahaan)
                                <option value="{{$perusahaan->id}}">{{$perusahaan->nama}}</option>
                            @endforeach
                            </select>
                            <label>Status</label>
                            <input type="text" id="status" name="status" class="form-control">
                            <label>Status karyawan</label>
                            <select class="form-control" name="statuspgw">
                                <option value="tetap">Tetap</option>
                                <option value="tidak tetap">Tidak tetap</option>
                                <option value="tenaga ahli">Tenaga ahli</option>
                            </select>
                            <label>Tanggal masuk</label>
                            <input type="text" id="tanggalmasuk" name="tanggalmasuk" class="form-control">
                            <label>NPWP</label>
                            <input type="text" id="npwp" name="npwp" class="form-control">
                            <label>NIK</label>
                            <input type="text" id="nik" name="nik" class="form-control">
                            <label>SIM</label>
                            <select class="form-control" name="jenissim">
                                <option value="A">A</option>
                                <option value="B1">B1</option>
                                <option value="B2">B2</option>
                                <option value="C">C</option>
                            </select>
                            <input type="text" id="nosim" name="nosim" class="form-control">
                            <label>Jabatan</label>
                            <input type="text" id="jabatan" name="jabatan" class="form-control">
                            <label>Proyek</label>
                            <input type="text" id="proyek" name="proyek" class="form-control">
                            <label>Masa kerja</label>
                            <input type="text" id="masakerja" name="masakerja" class="form-control">
                            <label>Alamat</label>
                            <input type="text" id="alamat" name="alamat" class="form-control">
                            <label>Nomor telpon</label>
                            <input type="text" id="telponrumah" name="telponrumah" class="form-control" placeholder="Telpon rumah">
                            <input type="text" id="telponmobile" name="telponmobile" class="form-control" placeholder="Mobile">
                            <label>Agama</label>
                            <input type="text" id="agama" name="agama" class="form-control">
                            <label>Gender</label>
                            <select class="form-control" name="gender">
                                <option value="laki-laki">Laki-laki</option>
                                <option value="perempuan">Perempuan</option>
                            </select>
                            <label>DOB</label>
                            <div class="input-group">
                                <input type="text" name="tempatlahir" class="form-control" placeholder="Tempat lahir">
                                <input type="text" name="tanggallahir" class="form-control" placeholder="Tanggal lahir">
                            </div>
                            <label>Golongan darah</label>
                            <select class="form-control" name="golongandarah">
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="AB">AB</option>
                                <option value="O">O</option>
                            </select>
                            <label>Pendidikan</label>
                            <div class="input-group">
                                <select class="form-control" id="studi" name="studi">
                                    @foreach($studis as $studi)
                                    <option value="{{$studi->id}}">{{$studi->jenjang}}</option>
                                    @endforeach
                                </select>
                                <input type="text" id="namaPen" name="namaPen" class="form-control" placeholder="Nama">
                                <input type="text" id="periodePen" name="periodePen" class="form-control" placeholder="Periode">
                            </div>
                            <input type="text" id="bidangstudi" name="bidangstudi" class="form-control" placeholder="Bidang studi" style="display: none;">
                            <input type="text" id="fakultas" name="fakultas" class="form-control" placeholder="Fakultas" style="display: none;">
                            <label>Kursus</label>
                            <input type="text" id="kursus" name="kursus" class="form-control">
                            <label>Pengalaman kerja</label>
                            <input type="text" id="namakerja" name="namakerja" class="form-control" placeholder="Perusahaan">
                            <input type="text" id="periodekerja" name="periodekerja" class="form-control" placeholder="Periode">
                            <input type="text" id="posisikerja" name="posisikerja" class="form-control" placeholder="Posisi">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-default form-control" type="submit">Submit</button>
                        </div>
                        <div style="color: red;">
                            @if ( $errors->count() > 0 )
                                <p>The following errors have occurred:</p>

                                <ul>
                                    @foreach( $errors->all() as $message )
                                        <li>{{ $message }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    {!! Html::script('js/form.js'); !!}
@endsection

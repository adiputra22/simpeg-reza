<style>
table {
    border-collapse: collapse;
    border: 1px solid red;
    margin-bottom: 1em;
    width: 100%;
}

table tr {
    page-break-inside: avoid;
}

table thead tr td {
    background-color: #F0F0F0;
    border: 1px solid #DDDDDD;
    min-width: 0.6em;
    padding: 5px;
    text-align: left;
    vertical-align: top;
    font-weight: bold;
}

table tbody tr td {
    border: 1px solid #DDDDDD;
    min-width: 0.6em;
    padding: 5px;
    vertical-align: top;
}

tbody tr.even td {
    background-color: transparent;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading" align="center">Data Pegawai Perusahaan {{ $perusahaan->nama }}</div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table border='1' id="report" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <td>No</td>
                                    <td>Data</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach($employeesData as $index => $employeeData): ?>
                                    <tr>
                                        <td align="center"><?php echo $no; ?></td>
                                        <td>
                                            <p>Data Pribadi</p>
                                            <table border="1" width="100%">
                                                <tbody>
                                                    <?php $cols = array_chunk($arrayMainSelect, ceil(count($arrayMainSelect)/8)); ?>
                                                    
                                                    <?php foreach($cols as $item): ?>
                                                        <tr>
                                                            <?php foreach($item as $select): ?>
                                                                <?php if( isset($employeesData[$index]['employee']->$select) ): ?>
                                                                    <td><b><?php echo strtoupper($select); ?></b></td>
                                                                    <td><?php echo strtoupper($employeesData[$index]['employee']->$select); ?></td>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                            </table>

                                            <?php if( array_key_exists('pendidikan', $requestData) && $requestData['pendidikan'] == "1" ): ?>
                                                <?php if( count($employeesData[$index]['employee_pendidikan'] > 0) ): ?>
                                                    <p>Data Pendidikan</p>
                                                    <table border="1" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <td>No</td>
                                                                <td>Jenjang</td>
                                                                <td>Nama</td>
                                                                <td>Periode</td>
                                                                <td>Bidang Studi</td>
                                                                <td>Fakultas</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach($employeesData[$index]['employee_pendidikan'] as $key => $item): ?>
                                                                <tr>
                                                                    <td><?php echo ($key + 1); ?></td>
                                                                    <td><?php echo $item->jenjang_name; ?></td>
                                                                    <td><?php echo $item->nama; ?></td>
                                                                    <td><?php echo $item->periode; ?></td>
                                                                    <td><?php echo $item->bidangstudi; ?></td>
                                                                    <td><?php echo $item->fakultas; ?></td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                <?php endif; ?>
                                            <?php endif; ?>

                                            <?php if( array_key_exists('kursus', $requestData) && $requestData['kursus'] == "1" ): ?>
                                                <?php if( count($employeesData[$index]['employee_kursus'] > 0) ): ?>
                                                    <p>Data Kursus</p>
                                                    <table border="1" width="100%" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <td>No</td>
                                                                <td>Nama Kursus</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach($employeesData[$index]['employee_kursus'] as $key => $item): ?>
                                                                <tr>
                                                                    <td><?php echo ($key + 1); ?></td>
                                                                    <td><?php echo $item->kursus; ?></td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                <?php endif; ?>
                                            <?php endif; ?>

                                            <?php if( array_key_exists('pekerjaan', $requestData) && $requestData['pekerjaan'] == "1" ): ?>
                                                <?php if( count($employeesData[$index]['employee_pekerjaan'] > 0) ): ?>
                                                    <p>Data Pekerjaan</p>
                                                    <table border="1" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <td>No</td>
                                                                <td>Nama Perusahaan</td>
                                                                <td>Periode</td>
                                                                <td>Posisi</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach($employeesData[$index]['employee_pekerjaan'] as $key => $item): ?>
                                                                <tr>
                                                                    <td><?php echo ($key + 1); ?></td>
                                                                    <td><?php echo $item->perusahaan_name; ?></td>
                                                                    <td><?php echo $item->periode; ?></td>
                                                                    <td><?php echo $item->posisi; ?></td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            
                                            <?php if( array_key_exists('sim', $requestData) && $requestData['sim'] == "1" ): ?>
                                                <?php if( count($employeesData[$index]['employee_sim'] > 0) ): ?>
                                                    <p>Data SIM</p>
                                                    <table border="1" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <td>No</td>
                                                                <td>Jenis SIM</td>
                                                                <td>Nomor SIM</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach($employeesData[$index]['employee_sim'] as $key => $item): ?>
                                                                <tr>
                                                                    <td><?php echo ($key + 1); ?></td>
                                                                    <td><?php echo $item->jenis; ?></td>
                                                                    <td><?php echo $item->nomorsim; ?></td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                <?php endif; ?>
                                            <?php endif; ?>

                                            <?php if( array_key_exists('mobile', $requestData) && $requestData['mobile'] == "1" ): ?>
                                                <?php if( count($employeesData[$index]['employee_mobile'] > 0) ): ?>
                                                    <p>Data Mobile</p>
                                                    <table border="1" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <td>No</td>
                                                                <td>Nomor</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach($employeesData[$index]['employee_mobile'] as $key => $item): ?>
                                                                <tr>
                                                                    <td><?php echo ($key + 1); ?></td>
                                                                    <td><?php echo $item->nomor; ?></td>
                                                                </tr>
                                                            <?php endforeach; ?>
                                                        </tbody>
                                                    </table>
                                                <?php endif; ?>
                                            <?php endif; ?>

                                        </td>

                                        <?php $no++; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <div>
                </div>
            </div>
        </div>
    </div>
</div>
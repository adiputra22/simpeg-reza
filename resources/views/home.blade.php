@extends('layouts.app')

@section('content')
<script src="{{ asset('vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

<script>
$(document).ready( function () {
    $('.tanggal').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      language: "id"
  });

  //Clone Pendidikan==================
    $(".tambahpendidikan").click(function(){
        var html = $(".clonependidikan").html();
        $(".incrementpendidikan").after(html);
    });

    $("body").on("click",".pendidikanhapus",function(){
        $(this).parents(".divpendidikan").remove();
    });
    //Clone Pendidikan==================

    //Clone Kursus==================
    $(".tambahkursus").click(function(){
        var html = $(".clonekursus").html();
        $(".incrementkursus").after(html);
    });

    $("body").on("click",".kursushapus",function(){
        $(this).parents(".divkursus").remove();
    });
    //Clone Kursus==================

    //Clone Pekerjaan==================
    $(".tambahpekerjaan").click(function(){
        var html = $(".clonepekerjaan").html();
        $(".incrementpekerjaan").after(html);
    });

    $("body").on("click",".pekerjaanhapus",function(){
        $(this).parents(".divpekerjaan").remove();
    });
    //Clone Pekerjaan==================

    //Clone SIM==================
    $(".tambahsim").click(function(){
        var html = $(".clonesim").html();
        $(".incrementsim").after(html);
    });

    $("body").on("click",".simhapus",function(){
        $(this).parents(".divsim").remove();
    });
    //Clone SIM==================

    //Clone Mobile==================
    $(".tambahmobile").click(function(){
        var html = $(".clonemobile").html();
        $(".incrementmobile").after(html);
    });

    $("body").on("click",".mobilehapus",function(){
        $(this).parents(".divmobile").remove();
    });
    //Clone Mobile==================
});
</script>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                @if (Session::has('message'))
                  <div class="col-md-12">
                    <div class="alert alert-info alert-dismissible" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      {{ Session::get('message') }}
                    </div>
                  </div>
                  @endif

                <div class="panel-body">
                    <form action="{{ url('/create') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                                <div class="col-md-12">
                                    <h3>Data Pegawai</h3>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" id="nama" name="nama" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Nama perusahaan</label>
                                    <select class="form-control" name="perusahaanid">
                                    @foreach($perusahaans as $perusahaan)
                                        <option value="{{$perusahaan->id}}">{{$perusahaan->nama}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                        <label>NIK</label>
                                        <input type="text" id="nik" name="nik" class="form-control" required>
                                </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                    <label>NPWP</label>
                                    <input type="text" id="npwp" name="npwp" class="form-control" required>

                            </div>
                            <div class="col-md-4">
                                    <label>Alamat</label>
                                        <input type="text" id="alamat" name="alamat" class="form-control">
                            </div>
                            <div class="col-md-4">
                                    <label>DOB</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" name="tempatlahir" class="form-control" placeholder="Tempat lahir">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" name="tanggallahir" class="form-control tanggal" placeholder="Tanggal lahir">
                                            </div>
                                        </div>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col-md-4">
                                        <label>Nomor telpon</label>
                                        <input type="text" id="telponrumah" name="telponrumah" class="form-control" placeholder="Telpon rumah">
                                </div>
                                <div class="col-md-4">
                                        <label>Status</label>
                                        <input type="text" id="status" name="status" class="form-control" >
                                </div>
                                <div class="col-md-4">
                                        <label>Status karyawan</label>
                                        <select class="form-control" name="statuspgw">
                                            <option value="tetap">Tetap</option>
                                            <option value="tidak tetap">Tidak tetap</option>
                                            <option value="tenaga ahli">Tenaga ahli</option>
                                        </select>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-md-4">
                                        <label>Tanggal masuk</label>
                                    <input type="date" id="tanggalmasuk" name="tanggalmasuk" class="form-control tanggal">

                                </div>
                                <div class="col-md-4">
                                        <label>Proyek</label>
                                        <input type="text" id="proyek" name="proyek" class="form-control">
                                </div>
                                <div class="col-md-4">
                                        <label>Masa Kerja</label>
                                        <input type="text" id="masakerja" name="masakerja" class="form-control">
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-md-4">
                                        <label>Jabatan</label>
                                        <input type="text" id="jabatan" name="jabatan" class="form-control">
                                </div>
                                <div class="col-md-4">
                                    &nbsp;
                                </div>
                                <div class="col-md-4">
                                    &nbsp;
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-md-4">
                                        <label>Agama</label>
                                        <input type="text" id="agama" name="agama" class="form-control">
                                </div>
                                <div class="col-md-4">
                                        <label>Golongan darah</label>
                                        <select class="form-control" name="golongandarah">
                                            <option value="A">A</option>
                                            <option value="B">B</option>
                                            <option value="AB">AB</option>
                                            <option value="O">O</option>
                                        </select>
                                </div>
                                <div class="col-md-4">
                                        <label>Gender</label>
                                        <select class="form-control" name="gender">
                                            <option value="laki-laki">Laki-laki</option>
                                            <option value="perempuan">Perempuan</option>
                                        </select>
                                </div>
                        </div>


                        <!-- Pendidikan -------------------------------------------------------------->
                        <div class="row">
                                <div class="col-md-12">
                                    <h3>Data Pendidikan</h3>
                                </div>
                        </div>
                        <div class="incrementpendidikan" >
                            <div class="row">
                                    <div class="col-md-4">
                                        <label>Jenjang Pendidikan</label>
                                        <select class="form-control" name="jenjangpendidikan[]">
                                                @foreach($studis as $s)
                                                <option value="{{$s->id}}">{{$s->jenjang}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Nama</label>
                                        <input type="text" id="nama" name="namapendidikan[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Periode</label>
                                        <input type="text" id="periode" name="periodependidikan[]" class="form-control">
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-4">
                                            <label>Bidang Studi</label>
                                            <input type="text" id="bidangstudi" name="bidangstudipendidikan[]" class="form-control" >
                                    </div>
                                    <div class="col-md-4">
                                            <label>Fakultas</label>
                                            <input type="text" id="fakultas" name="fakultaspendidikan[]" class="form-control" >
                                    </div>
                                    <div class="col-md-4">
                                            <div class="col-md-1" style="margin-top: 25px; margin-left: -13px">
                                                    <button class="btn btn-success tambahpendidikan" type="button"><i class="glyphicon glyphicon-plus"></i>Tambah</button>
                                                </div>
                                    </div>
                            </div>
                        </div>
                        <div class="clonependidikan hide">
                            <div class="divpendidikan">
                            <div class="row">
                                    <div class="col-md-4">
                                        <label>Jenjang Pendidikan</label>
                                        <select class="form-control" name="jenjangpendidikan[]">
                                                @foreach($studis as $s)
                                                <option value="{{$s->id}}">{{$s->jenjang}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Nama</label>
                                        <input type="text" id="nama" name="namapendidikan[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Periode</label>
                                        <input type="text" id="periode" name="periodependidikan[]" class="form-control">
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-4">
                                            <label>Bidang Studi</label>
                                            <input type="text" id="bidangstudi" name="bidangstudipendidikan[]" class="form-control" >
                                    </div>
                                    <div class="col-md-4">
                                            <label>Fakultas</label>
                                            <input type="text" id="fakultas" name="fakultaspendidikan[]" class="form-control" >
                                    </div>
                                    <div class="col-md-4">
                                            <div class="col-sm-1" style="margin-top: 25px; margin-left: -13px;">
                                                    <div class="input-group-btn">
                                                      <button class="btn btn-danger pendidikanhapus" type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                                    </div>
                                                  </div>
                                    </div>
                            </div>
                            </div>
                        </div>


                        <!-- Kursus -------------------------------------------------------------->
                        <div class="incrementkursus" >
                            <div class="row">
                                    <div class="col-md-12">
                                        <h3>Data Kursus</h3>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-8">
                                            <label>Nama Kursus</label>
                                            <input type="text" id="kursus" name="namakursus[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                            <div class="col-md-1" style="margin-top: 25px; margin-left: -13px">
                                                    <button class="btn btn-success tambahkursus" type="button"><i class="glyphicon glyphicon-plus"></i>Tambah</button>
                                                </div>
                                    </div>
                            </div>
                        </div>
                        <div class="clonekursus hide">
                                <div class="divkursus">
                                        <div class="row">
                                                <div class="col-md-8">
                                                        <label>Nama Kursus</label>
                                                        <input type="text" id="kursus" name="namakursus[]" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                        <div class="col-sm-1" style="margin-top: 25px; margin-left: -13px;">
                                                                <div class="input-group-btn">
                                                                  <button class="btn btn-danger kursushapus" type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                                                </div>
                                                              </div>
                                                </div>
                                        </div>
                                </div>
                        </div>


                        <!-- Pekerjaan -------------------------------------------------------------->
                        <div class="incrementpekerjaan" >
                            <div class="row">
                                    <div class="col-md-12">
                                        <h3>Data Pekerjaan</h3>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-4">
                                            <label>Perusahaan</label>
                                            <input type="text" id="nomor" name="perusahaan[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                            <label>Posisi</label>
                                            <input type="text" id="posisi" name="posisi[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-8">
                                                    <label>Periode</label>
                                                    <input type="text" id="periode" name="periodepekerjaan[]" class="form-control">
                                            </div>
                                            <div class="col-md-4">
                                                    <div class="col-md-1" style="margin-top: 25px; margin-left: -33px">
                                                            <button class="btn btn-success tambahpekerjaan" type="button"><i class="glyphicon glyphicon-plus"></i>Tambah</button>
                                                        </div>
                                                </div>
                                        </div>

                                    </div>
                            </div>
                        </div>
                        <div class="clonepekerjaan hide">
                                <div class="divpekerjaan">
                                        <div class="row">
                                                <div class="col-md-4">
                                                        <label>Perusahaan</label>
                                                        <input type="text" id="nomor" name="perusahaan[]" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                        <label>Posisi</label>
                                                        <input type="text" id="posisi" name="posisi[]" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                                <label>Periode</label>
                                                                <input type="text" id="periode" name="periodepekerjaan[]" class="form-control">
                                                        </div>
                                                        <div class="col-md-4">
                                                                <div class="col-sm-1" style="margin-top: 25px; margin-left: -30px;">
                                                                        <div class="input-group-btn">
                                                                          <button class="btn btn-danger pekerjaanhapus" type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                                                        </div>
                                                                      </div>
                                                            </div>
                                                    </div>

                                                </div>
                                        </div>
                                </div>
                        </div>


                        <!-- SIM -------------------------------------------------------------->
                        <div class="incrementsim" >
                            <div class="row">
                                    <div class="col-md-12">
                                        <h3>Data SIM</h3>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-4">
                                            <label>SIM</label>
                                            <select class="form-control" name="jenissim[]">
                                                <option value="A">A</option>
                                                <option value="B1">B1</option>
                                                <option value="B2">B2</option>
                                                <option value="C">C</option>
                                            </select>
                                    </div>
                                    <div class="col-md-4">
                                            <label>No. SIM</label>
                                            <input type="text" id="nosim" name="nosim[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-md-1" style="margin-top: 25px; margin-left: -33px">
                                                <button class="btn btn-success tambahsim" type="button"><i class="glyphicon glyphicon-plus"></i>Tambah</button>
                                            </div>

                                    </div>
                            </div>
                        </div>
                        <div class="clonesim hide">
                                <div class="divsim">
                                        <div class="row">
                                                <div class="col-md-4">
                                                        <label>SIM</label>
                                                        <select class="form-control" name="jenissim[]">
                                                            <option value="A">A</option>
                                                            <option value="B1">B1</option>
                                                            <option value="B2">B2</option>
                                                            <option value="C">C</option>
                                                        </select>
                                                </div>
                                                <div class="col-md-4">
                                                        <label>No. SIM</label>
                                                        <input type="text" id="nosim" name="nosim[]" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                        <div class="col-sm-1" style="margin-top: 25px; margin-left: -29px;">
                                                                <div class="input-group-btn">
                                                                  <button class="btn btn-danger simhapus" type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                                                </div>
                                                              </div>

                                                </div>
                                        </div>
                                </div>
                        </div>


                        <!-- Mobile -------------------------------------------------------------->
                        <div class="incrementmobile" >
                            <div class="row">
                                    <div class="col-md-12">
                                        <h3>Data Mobile</h3>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-8">
                                            <label>Nomer HP</label>
                                            <input type="text" id="nomor" name="nomormobile[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-md-1" style="margin-top: 25px; margin-left: -33px">
                                                <button class="btn btn-success tambahmobile" type="button"><i class="glyphicon glyphicon-plus"></i>Tambah</button>
                                            </div>

                                    </div>
                            </div>
                        </div>
                        <div class="clonemobile hide">
                                <div class="divmobile">
                                        <div class="row">
                                                <div class="col-md-8">
                                                        <label>Nomer HP</label>
                                                        <input type="text" id="nomor" name="nomormobile[]" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                        <div class="col-sm-1" style="margin-top: 25px; margin-left: -29px;">
                                                                <div class="input-group-btn">
                                                                  <button class="btn btn-danger mobilehapus" type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                                                </div>
                                                              </div>

                                                </div>
                                        </div>
                                </div>
                        </div>



                        <hr>
                        <div class="form-group">
                            <button class="btn btn-primary form-control" type="submit">Submit</button>
                        </div>

                        <div style="color: red;">
                            @if ( $errors->count() > 0 )
                                <p>The following errors have occurred:</p>

                                <ul>
                                    @foreach( $errors->all() as $message )
                                        <li>{{ $message }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    {!! Html::script('js/form.js'); !!}
@endsection

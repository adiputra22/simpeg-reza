@extends('layouts.adminlte')

@section('content')
<section class="content-header">
      <h1>
        Data Pegawai
        <small>Edit data pegawai berdasarkan perusahaan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Data Pegawai</li>
      </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <form action="{{ url('/update') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="nip" value="{{$nip}}" >
                        <div class="row">
                                <div class="col-md-12">
                                    <h3>Data Pegawai</h3>
                                </div>
                        </div>
                        <div class="row">
                                @if(Request::input('nama'))
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" id="nama" name="nama" class="form-control" value="{{$pegawai->nama}}">
                                    </div>
                                </div>
                                @else
                                <input type="hidden" name="nama" value="{{$pegawai->nama}}" >
                                @endif

                                @if(Request::input('perusahaanid'))
                                <div class="col-md-4">
                                    <label>Nama perusahaan</label>
                                    <select class="form-control" name="perusahaanid">
                                    @foreach($perusahaans as $perusahaan)
                                        <option value="{{$perusahaan->id}}"  @if($pegawai->perusahaan==$perusahaan->id) selected @endif>{{$perusahaan->nama}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                @else
                                <input type="hidden" name="perusahaanid" value="{{$pegawai->perusahaan}}" >
                                @endif
                                @if(Request::input('nik'))
                                <div class="col-md-4">
                                        <label>NIK</label>
                                        <input type="text" id="nik" name="nik" class="form-control" value="{{$pegawai->nik}}">
                                </div>
                                @else
                                <input type="hidden" name="nik" value="{{$pegawai->nik}}" >
                                @endif
                        </div>
                        <div class="row">
                            @if(Request::input('npwp'))
                            <div class="col-md-4">
                                    <label>NPWP</label>
                                    <input type="text" id="npwp" name="npwp" class="form-control" value="{{$pegawai->npwp}}">

                            </div>
                            @else
                                <input type="hidden" name="npwp" value="{{$pegawai->npwp}}" >
                            @endif
                            @if(Request::input('alamat'))
                            <div class="col-md-4">
                                    <label>Alamat</label>
                                        <input type="text" id="alamat" name="alamat" class="form-control" value="{{$pegawai->alamat}}">
                            </div>
                            @else
                                <input type="hidden" name="alamat" value="{{$pegawai->alamat}}" >
                            @endif

                            @if(Request::input('tempatlahir') || Request::input('tanggallahir'))
                            <div class="col-md-4">
                                    <label>DOB</label>
                                        <div class="row">
                                            @if(Request::input('tempatlahir'))
                                            <div class="col-md-6">
                                                <input type="text" id="tempatlahir" name="tempatlahir" class="form-control" placeholder="Tempat lahir" value="{{$pegawai->tempatlahir}}">
                                            </div>
                                            @else
                                                <input type="hidden" name="tempatlahir" value="{{$pegawai->tempatlahir}}" >
                                            @endif
                                            @if(Request::input('tanggallahir'))
                                            <div class="col-md-6">
                                                <input type="text" id="tanggallahir" name="tanggallahir" class="form-control tanggal" placeholder="Tanggal lahir" value="{{$pegawai->tanggallahir}}">
                                            </div>
                                            @else
                                                <input type="hidden" name="tanggallahir" value="{{$pegawai->tanggallahir}}" >
                                            @endif
                                        </div>
                            </div>
                            @else
                            <input type="hidden" name="tempatlahir" value="{{$pegawai->tempatlahir}}" >
                            <input type="hidden" name="tanggallahir" value="{{$pegawai->tanggallahir}}" >
                            @endif
                        </div>
                        <div class="row">
                                @if(Request::input('telponrumah'))
                                <div class="col-md-4">
                                        <label>Nomor telpon</label>
                                        <input type="text" id="telponrumah" name="telponrumah" class="form-control" placeholder="Telpon rumah" value="{{$pegawai->telponrumah}}">
                                </div>
                                @else
                                <input type="hidden" name="telponrumah" value="{{$pegawai->telponrumah}}" >
                            @endif
                            @if(Request::input('status'))
                                <div class="col-md-4">
                                        <label>Status</label>
                                        <input type="text" id="status" name="status" class="form-control" value="{{$pegawai->status}}">
                                </div>
                                @else
                                <input type="hidden" name="status" value="{{$pegawai->status}}" >
                            @endif
                            @if(Request::input('statuskaryawan'))
                                <div class="col-md-4">
                                        <label>Status karyawan</label>
                                        <select class="form-control" name="statuspgw">
                                            <option value="tetap" @if($pegawai->statuskaryawan=="tetap") selected @endif>Tetap</option>
                                            <option value="tidak tetap" @if($pegawai->statuskaryawan=="tidak tetap") selected @endif>Tidak tetap</option>
                                            <option value="tenaga ahli" @if($pegawai->statuskaryawan=="tenaga ahli") selected @endif>Tenaga ahli</option>
                                        </select>
                                </div>
                                @else
                                <input type="hidden" name="statuspgw" value="{{$pegawai->statuskaryawan}}" >
                            @endif
                        </div>
                        <div class="row">
                                @if(Request::input('tanggalmasuk'))
                                <div class="col-md-4">
                                        <label>Tanggal masuk</label>
                                    <input type="text" id="tanggalmasuk" name="tanggalmasuk" class="form-control tanggal" value="{{$pegawai->tanggalmasuk}}">

                                </div>
                                @else
                                    <input type="hidden" name="tanggalmasuk" value="{{$pegawai->tanggalmasuk}}" >
                                @endif
                                @if(Request::input('proyek'))
                                <div class="col-md-4">
                                        <label>Proyek</label>
                                        <input type="text" id="proyek" name="proyek" class="form-control" value="{{$pegawai->proyek}}">
                                </div>
                                @else
                                    <input type="hidden" name="proyek" value="{{$pegawai->proyek}}" >
                                @endif
                                @if(Request::input('masakerja'))
                                <div class="col-md-4">
                                        <label>Masa Kerja</label>
                                        <input type="text" id="masakerja" name="masakerja" class="form-control" value="{{$pegawai->masakerja}}">
                                </div>
                                @else
                                    <input type="hidden" name="masakerja" value="{{$pegawai->masakerja}}" >
                                @endif
                        </div>
                        <div class="row">
                                @if(Request::input('jabatan'))
                                <div class="col-md-4">
                                        <label>Jabatan</label>
                                        <input type="text" id="jabatan" name="jabatan" class="form-control" value="{{$pegawai->jabatan}}">
                                </div>
                                @else
                                    <input type="hidden" name="jabatan" value="{{$pegawai->jabatan}}" >
                                @endif
                                @if(Request::input('agama'))
                                <div class="col-md-4">
                                        <label>Agama</label>
                                        <input type="text" id="agama" name="agama" class="form-control" value="{{$pegawai->agama}}">
                                </div>
                                @else
                                    <input type="hidden" name="agama" value="{{$pegawai->agama}}" >
                                @endif
                                @if(Request::input('golongandarah'))
                                <div class="col-md-4">
                                        <label>Golongan darah</label>
                                        <select class="form-control" name="golongandarah">
                                            <option value="A" @if($pegawai->golongandarah=="A") selected @endif>A</option>
                                            <option value="B" @if($pegawai->golongandarah=="B") selected @endif>B</option>
                                            <option value="AB" @if($pegawai->golongandarah=="AB") selected @endif>AB</option>
                                            <option value="O" @if($pegawai->golongandarah=="0") selected @endif>O</option>
                                        </select>
                                </div>
                                @else
                                    <input type="hidden" name="golongandarah" value="{{$pegawai->golongandarah}}" >
                                @endif
                        </div>
                        <div class="row">
                                @if(Request::input('gender'))
                                <div class="col-md-4">
                                        <label>Gender</label>
                                        <select class="form-control" name="gender">
                                            <option value="laki-laki" @if($pegawai->gender=="laki-laki") selected @endif>Laki-laki</option>
                                            <option value="perempuan" @if($pegawai->gender=="perempuan") selected @endif>Perempuan</option>
                                        </select>
                                </div>
                                @else
                                    <input type="hidden" name="gender" value="{{$pegawai->gender}}" >
                                @endif
                                <div class="col-md-4">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-4">
                                        &nbsp;
                                    </div>
                        </div>


                        <!-- Pendidikan -------------------------------------------------------------->
                        @if(Request::input('pendidikan'))
                        <input type="hidden" name="datapendidikan" value="1" >
                        <div class="row">
                                <div class="col-md-12">
                                    <h3>Data Pendidikan</h3>
                                </div>
                        </div>
                        @foreach($pendidikan as $pend)
                        <div class="row">
                                <div class="col-md-4">
                                    <label>Jenjang Pendidikan</label>
                                    <select class="form-control" name="jenjangpendidikan[]">
                                            @foreach($studis as $s)
                                            <option value="{{$s->id}}" @if($s->id==$pend->jenjang) selected @endif>{{$s->jenjang}}</option>
                                            @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label>Nama</label>
                                    <input type="text" id="nama" name="namapendidikan[]" class="form-control" value="{{$pend->nama}}">
                                </div>
                                <div class="col-md-4">
                                    <label>Periode</label>
                                    <input type="text" id="periode" name="periodependidikan[]" class="form-control" value="{{$pend->periode}}">
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-md-4">
                                        <label>Bidang Studi</label>
                                        <input type="text" id="bidangstudi" name="bidangstudipendidikan[]" class="form-control" value="{{$pend->bidangstudi}}">
                                </div>
                                <div class="col-md-4">
                                        <label>Fakultas</label>
                                        <input type="text" id="fakultas" name="fakultaspendidikan[]" class="form-control" value="{{$pend->fakultas}}">
                                </div>
                                <div class="col-md-4">
                                        &nbsp;
                                </div>
                        </div>
                        @endforeach
                        <div class="incrementpendidikan" style="background-color: #f1e2de">
                            <div class="row">
                                    <div class="col-md-4">
                                        <label>Jenjang Pendidikan</label>
                                        <select class="form-control" name="jenjangpendidikan[]">
                                                @foreach($studis as $s)
                                                <option value="{{$s->id}}" @if($s->id==$pend->jenjang) selected @endif>{{$s->jenjang}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Nama</label>
                                        <input type="text" id="nama" name="namapendidikan[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Periode</label>
                                        <input type="text" id="periode" name="periodependidikan[]" class="form-control">
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-4">
                                            <label>Bidang Studi</label>
                                            <input type="text" id="bidangstudi" name="bidangstudipendidikan[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                            <label>Fakultas</label>
                                            <input type="text" id="fakultas" name="fakultaspendidikan[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                            <div class="col-md-1" style="margin-top: 25px; margin-left: -13px">
                                                    <button class="btn btn-success tambahpendidikan" type="button"><i class="glyphicon glyphicon-plus"></i>Tambah</button>
                                                </div>
                                    </div>
                            </div>
                        </div>
                        <div class="clonependidikan hide">
                            <div class="divpendidikan" style="background-color: #f1e2de">
                            <div class="row">
                                    <div class="col-md-4">
                                        <label>Jenjang Pendidikan</label>
                                        <select class="form-control" name="jenjangpendidikan[]">
                                                @foreach($studis as $s)
                                                <option value="{{$s->id}}" @if($s->id==$pend->jenjang) selected @endif>{{$s->jenjang}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Nama</label>
                                        <input type="text" id="nama" name="namapendidikan[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <label>Periode</label>
                                        <input type="text" id="periode" name="periodependidikan[]" class="form-control">
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-4">
                                            <label>Bidang Studi</label>
                                            <input type="text" id="bidangstudi" name="bidangstudipendidikan[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                            <label>Fakultas</label>
                                            <input type="text" id="fakultas" name="fakultaspendidikan[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                            <div class="col-sm-1" style="margin-top: 25px; margin-left: -13px;">
                                              <div class="input-group-btn">
                                                <button class="btn btn-danger pendidikanhapus" type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                              </div>
                                           </div>
                                    </div>
                            </div>
                            </div>
                        </div>

                        @else
                            <input type="hidden" name="datapendidikan" value="0" >
                        @endif
                        <!-- Kursus -------------------------------------------------------------->
                        @if(Request::input('kursus'))
                        <input type="hidden" name="datakursus" value="1" >
                            <div class="row">
                                    <div class="col-md-12">
                                        <h3>Data Kursus</h3>
                                    </div>
                            </div>
                            @foreach($kursus as $kurs)
                            <div class="row">
                                    <div class="col-md-8">
                                            <label>Nama Kursus</label>
                                            <input type="text" id="kursus" name="namakursus[]" class="form-control" value="{{$kurs->kursus}}">
                                    </div>
                                    <div class="col-md-4">
                                            &nbsp;
                                    </div>
                            </div>
                            @endforeach
                            <div class="incrementkursus" style="background-color: #f1e2de">
                            <div class="row">
                                    <div class="col-md-8">
                                            <label>Nama Kursus</label>
                                            <input type="text" id="kursus" name="namakursus[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                            <div class="col-md-1" style="margin-top: 25px; margin-left: -13px">
                                                    <button class="btn btn-success tambahkursus" type="button"><i class="glyphicon glyphicon-plus"></i>Tambah</button>
                                                </div>
                                    </div>
                            </div>
                        </div>
                        <div class="clonekursus hide">
                                <div class="divkursus" style="background-color: #f1e2de">
                                        <div class="row">
                                                <div class="col-md-8">
                                                        <label>Nama Kursus</label>
                                                        <input type="text" id="kursus" name="namakursus[]" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                        <div class="col-sm-1" style="margin-top: 25px; margin-left: -13px;">
                                                            <div class="input-group-btn">
                                                              <button class="btn btn-danger kursushapus" type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                                            </div>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        @else
                            <input type="hidden" name="datakursus" value="0" >
                        @endif

                        <!-- Pekerjaan -------------------------------------------------------------->
                        @if(Request::input('pekerjaan'))
                        <input type="hidden" name="datakerja" value="1" >
                            <div class="row">
                                    <div class="col-md-12">
                                        <h3>Data Pekerjaan</h3>
                                    </div>
                            </div>
                            @foreach($kerja as $kerj)
                            <div class="row">
                                    <div class="col-md-4">
                                            <label>Perusahaan</label>
                                            <input type="text" id="perusahaan" name="perusahaan[]" class="form-control" value="{{$kerj->perusahaan}}">
                                    </div>
                                    <div class="col-md-4">
                                            <label>Posisi</label>
                                            <input type="text" id="posisi" name="posisi[]" class="form-control" value="{{$kerj->posisi}}">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-8">
                                                    <label>Periode</label>
                                                    <input type="text" id="periode" name="periodepekerjaan[]" class="form-control" value="{{$kerj->periode}}">
                                            </div>
                                            <div class="col-md-4">
                                                    &nbsp;
                                                </div>
                                        </div>

                                    </div>
                            </div>
                            @endforeach

                            <div class="incrementpekerjaan" style="background-color: #f1e2de" >
                            <div class="row">
                                    <div class="col-md-4">
                                            <label>Perusahaan</label>
                                            <input type="text" id="perusahaan" name="perusahaan[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                            <label>Posisi</label>
                                            <input type="text" id="posisi" name="posisi[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-8">
                                                    <label>Periode</label>
                                                    <input type="text" id="periode" name="periodepekerjaan[]" class="form-control">
                                            </div>
                                            <div class="col-md-4">
                                                    <div class="col-md-1" style="margin-top: 25px; margin-left: -33px">
                                                            <button class="btn btn-success tambahpekerjaan" type="button"><i class="glyphicon glyphicon-plus"></i>Tambah</button>
                                                        </div>
                                                </div>
                                        </div>

                                    </div>
                            </div>
                        </div>
                        <div class="clonepekerjaan hide">
                                <div class="divpekerjaan" style="background-color: #f1e2de">
                                        <div class="row">
                                                <div class="col-md-4">
                                                        <label>Perusahaan</label>
                                                        <input type="text" id="perusahaan" name="perusahaan[]" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                        <label>Posisi</label>
                                                        <input type="text" id="posisi" name="posisi[]" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                                <label>Periode</label>
                                                                <input type="text" id="periode" name="periodepekerjaan[]" class="form-control">
                                                        </div>
                                                        <div class="col-md-4">
                                                                <div class="col-sm-1" style="margin-top: 25px; margin-left: -30px;">
                                                                        <div class="input-group-btn">
                                                                          <button class="btn btn-danger pekerjaanhapus" type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                                                        </div>
                                                                      </div>
                                                            </div>
                                                    </div>

                                                </div>
                                        </div>
                                </div>
                        </div>
                        @else
                            <input type="hidden" name="datakerja" value="0" >
                        @endif

                        <!-- SIM -------------------------------------------------------------->
                        @if(Request::input('sim'))
                        <input type="hidden" name="datasim" value="1" >
                            <div class="row">
                                    <div class="col-md-12">
                                        <h3>Data SIM</h3>
                                    </div>
                            </div>
                            @foreach($sim as $si)
                            <div class="row">
                                    <div class="col-md-4">
                                            <label>SIM</label>
                                            <select class="form-control" name="jenissim[]">
                                                <option value="A" @if($si->jenis=="A") selected @endif>A</option>
                                                <option value="B1" @if($si->jenis=="B1") selected @endif>B1</option>
                                                <option value="B2" @if($si->jenis=="B2") selected @endif>B2</option>
                                                <option value="C" @if($si->jenis=="C") selected @endif>C</option>
                                            </select>
                                    </div>
                                    <div class="col-md-4">
                                            <label>No. SIM</label>
                                            <input type="text" id="nosim" name="nosim[]" class="form-control" value="{{$si->nomorsim}}">
                                    </div>
                                    <div class="col-md-4">
                                        &nbsp;

                                    </div>
                            </div>
                            @endforeach
                            <div class="incrementsim" style="background-color: #f1e2de" >
                            <div class="row">
                                    <div class="col-md-4">
                                            <label>SIM</label>
                                            <select class="form-control" name="jenissim[]">
                                                <option value="A">A</option>
                                                <option value="B1">B1</option>
                                                <option value="B2">B2</option>
                                                <option value="C">C</option>
                                            </select>
                                    </div>
                                    <div class="col-md-4">
                                            <label>No. SIM</label>
                                            <input type="text" id="nosim" name="nosim[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-md-1" style="margin-top: 25px; margin-left: -33px">
                                                <button class="btn btn-success tambahsim" type="button"><i class="glyphicon glyphicon-plus"></i>Tambah</button>
                                            </div>

                                    </div>
                            </div>
                        </div>
                        <div class="clonesim hide">
                                <div class="divsim" style="background-color: #f1e2de">
                                        <div class="row">
                                                <div class="col-md-4">
                                                        <label>SIM</label>
                                                        <select class="form-control" name="jenissim[]">
                                                            <option value="A">A</option>
                                                            <option value="B1">B1</option>
                                                            <option value="B2">B2</option>
                                                            <option value="C">C</option>
                                                        </select>
                                                </div>
                                                <div class="col-md-4">
                                                        <label>No. SIM</label>
                                                        <input type="text" id="nosim" name="nosim[]" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                        <div class="col-sm-1" style="margin-top: 25px; margin-left: -29px;">
                                                                <div class="input-group-btn">
                                                                  <button class="btn btn-danger simhapus" type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                                                </div>
                                                              </div>

                                                </div>
                                        </div>
                                </div>
                        </div>
                        @else
                            <input type="hidden" name="datasim" value="0" >
                        @endif

                        <!-- Mobile -------------------------------------------------------------->
                        @if(Request::input('mobile'))
                        <input type="hidden" name="datamobile" value="1" >
                            <div class="row">
                                    <div class="col-md-12">
                                        <h3>Data Mobile</h3>
                                    </div>
                            </div>
                            @foreach($mobile as $mob)
                            <div class="row">
                                    <div class="col-md-8">
                                            <label>Nomer HP</label>
                                            <input type="text" id="nomor" name="nomormobile[]" class="form-control" value="{{$mob->nomor}}">
                                    </div>
                                    <div class="col-md-4">
                                        &nbsp;

                                    </div>
                            </div>
                            @endforeach

                            <div class="incrementmobile" style="background-color: #f1e2de" >
                            <div class="row">
                                    <div class="col-md-8">
                                            <label>Nomer HP</label>
                                            <input type="text" id="nomor" name="nomormobile[]" class="form-control">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="col-md-1" style="margin-top: 25px; margin-left: -33px">
                                                <button class="btn btn-success tambahmobile" type="button"><i class="glyphicon glyphicon-plus"></i>Tambah</button>
                                            </div>

                                    </div>
                            </div>
                        </div>
                        <div class="clonemobile hide">
                                <div class="divmobile" style="background-color: #f1e2de">
                                        <div class="row">
                                                <div class="col-md-8">
                                                        <label>Nomer HP</label>
                                                        <input type="text" id="nomor" name="nomormobile[]" class="form-control">
                                                </div>
                                                <div class="col-md-4">
                                                        <div class="col-sm-1" style="margin-top: 25px; margin-left: -29px;">
                                                                <div class="input-group-btn">
                                                                  <button class="btn btn-danger mobilehapus" type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                                                </div>
                                                              </div>

                                                </div>
                                        </div>
                                </div>
                        </div>
                        @else
                            <input type="hidden" name="datamobile" value="0" >
                        @endif


                        <hr>
                        <div class="row">
                                <div class="col-md-2">
                                        <button type="button" class="btn btn-default form-control" onclick="location.href='/view'">Kembali</button>
                                </div>
                                <div class="col-md-2">
                                        <button class="btn btn-primary form-control" type="submit">Update</button>
                                </div>
                                <div class="col-md-2">
                                        <button class="btn btn-danger form-control" type="button"  onclick="location.href='/delete/{{$nip}}'">Delete</button>
                                </div>
                        </div>
                        <div style="color: red;">
                            @if ( $errors->count() > 0 )
                                <p>The following errors have occurred:</p>

                                <ul>
                                    @foreach( $errors->all() as $message )
                                        <li>{{ $message }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

    <script>
    $(document).ready( function () {
        $('.tanggal').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            language: "id"
        });

        //Clone Pendidikan==================
        $(".tambahpendidikan").click(function(){
            var html = $(".clonependidikan").html();
            $(".incrementpendidikan").after(html);
        });

        $("body").on("click",".pendidikanhapus",function(){
            $(this).parents(".divpendidikan").remove();
        });
        //Clone Pendidikan==================

        //Clone Kursus==================
        $(".tambahkursus").click(function(){
            var html = $(".clonekursus").html();
            $(".incrementkursus").after(html);
        });

        $("body").on("click",".kursushapus",function(){
            $(this).parents(".divkursus").remove();
        });
        //Clone Kursus==================

        //Clone Pekerjaan==================
        $(".tambahpekerjaan").click(function(){
            var html = $(".clonepekerjaan").html();
            $(".incrementpekerjaan").after(html);
        });

        $("body").on("click",".pekerjaanhapus",function(){
            $(this).parents(".divpekerjaan").remove();
        });
        //Clone Pekerjaan==================

        //Clone SIM==================
        $(".tambahsim").click(function(){
            var html = $(".clonesim").html();
            $(".incrementsim").after(html);
        });

        $("body").on("click",".simhapus",function(){
            $(this).parents(".divsim").remove();
        });
        //Clone SIM==================

        //Clone Mobile==================
        $(".tambahmobile").click(function(){
            var html = $(".clonemobile").html();
            $(".incrementmobile").after(html);
        });

        $("body").on("click",".mobilehapus",function(){
            $(this).parents(".divmobile").remove();
        });
        //Clone Mobile==================
    });
</script>

{!! Html::script('js/form.js'); !!}
@endsection
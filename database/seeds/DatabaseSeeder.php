<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('perusahaan')->insert([
            'nama' => 'MRI',
        ]);
        DB::table('perusahaan')->insert([
            'nama' => 'SPI',
        ]);
        DB::table('perusahaan')->insert([
            'nama' => 'UBS',
        ]);
        DB::table('perusahaan')->insert([
            'nama' => 'MII',
        ]);
        
        DB::table('studi')->insert([
            'jenjang' => 'SD',
        ]);
        DB::table('studi')->insert([
            'jenjang' => 'SMP',
        ]);
        DB::table('studi')->insert([
            'jenjang' => 'SMA',
        ]);
        DB::table('studi')->insert([
            'jenjang' => 'D1',
        ]);
        DB::table('studi')->insert([
            'jenjang' => 'D2',
        ]);
        DB::table('studi')->insert([
            'jenjang' => 'D3',
        ]);
        DB::table('studi')->insert([
            'jenjang' => 'S1',
        ]);
        DB::table('studi')->insert([
            'jenjang' => 'S2',
        ]);
        DB::table('studi')->insert([
            'jenjang' => 'S3',
        ]);
        
        DB::table('main')->insert([
            'nama' => 'test',
            'perusahaan' => 1,
            'nik' => '0554450455',
            'npwp' => '0554450455',
            'alamat' => 'Jalan jalan kompleks komplek',
            'tempatlahir' => 'Jakarta',
            'tanggallahir' => Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'),
            'telponrumah' => '093039403',
            'status' => '',
            'statuskaryawan' => 'Tidak tetap',
            'tanggalmasuk' => Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'),
            'jabatan' => 'penganggur',
            'proyek' => '---',
            'masakerja' => '2018-2019',
            'agama' => 'agama',
            'gender' => 'laki-laki',
            'golongandarah' => 'O',
        ]);
        
        DB::table('pendidikan')->insert([
            'no' => 1,
            'jenjang' => 1,
            'nama' => 'SD Sedih',
            'periode' => '2000',
            'bidangstudi' => '',
            'fakultas' => '',
        ]);
        
        DB::table('pendidikan')->insert([
            'no' => 1,
            'jenjang' => 2,
            'nama' => 'SMP Sedih',
            'periode' => '2010',
            'bidangstudi' => '',
            'fakultas' => '',
        ]);
        
        DB::table('kerja')->insert([
            'no' => 1,
            'perusahaan' => 'A',
            'periode' => '2000-2010',
            'posisi' => 'Pajangan',
        ]);
        
        DB::table('kursus')->insert([
            'no' => 1,
            'kursus' => 'Menambal ban',
        ]);
        
        DB::table('mobile')->insert([
            'no' => 1,
            'nomor' => '093453145',
        ]);
        
        DB::table('mobile')->insert([
            'no' => 1,
            'nomor' => '103453145',
        ]);
        
        DB::table('sim')->insert([
            'no' => 1,
            'jenis' => 'A',
            'nomorsim' => '093453145',
        ]);
        
        //
        DB::table('main')->insert([
            'nama' => 'test',
            'perusahaan' => 3,
            'nik' => '0552350455',
            'npwp' => '0554451455',
            'alamat' => 'Jalan jalan kompleks komplek',
            'tempatlahir' => 'Jakarta',
            'tanggallahir' => Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'),
            'telponrumah' => '093119403',
            'status' => '',
            'statuskaryawan' => 'Tidak tetap',
            'tanggalmasuk' => Carbon::now('Asia/Jakarta')->format('Y-m-d H:i:s'),
            'jabatan' => 'penganggur',
            'proyek' => '---',
            'masakerja' => '2010-2011',
            'agama' => 'agama',
            'gender' => 'laki-laki',
            'golongandarah' => 'AB',
        ]);
        
        DB::table('pendidikan')->insert([
            'no' => 2,
            'jenjang' => 1,
            'nama' => 'SD Sedih',
            'periode' => '1500',
            'bidangstudi' => '',
            'fakultas' => '',
        ]);
        
        DB::table('kerja')->insert([
            'no' => 2,
            'perusahaan' => 'A',
            'periode' => '1900-2010',
            'posisi' => 'Pajangan',
        ]);
        
        DB::table('kursus')->insert([
            'no' => 2,
            'kursus' => 'kursus 1',
        ]);
        
        DB::table('mobile')->insert([
            'no' => 2,
            'nomor' => '093453146',
        ]);
        
        DB::table('sim')->insert([
            'no' => 2,
            'jenis' => 'B2',
            'nomorsim' => '093453146',
        ]);
    }
}

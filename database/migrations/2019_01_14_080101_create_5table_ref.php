<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create5tableRef extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pendidikan', function (Blueprint $table) {
            $table->foreign('no')->references('no')->on('main');
        });
        
        Schema::table('kursus', function (Blueprint $table) {
            $table->foreign('no')->references('no')->on('main');
        });
        
        Schema::table('kerja', function (Blueprint $table) {
            $table->foreign('no')->references('no')->on('main');
        });
        
        Schema::table('sim', function (Blueprint $table) {
            $table->foreign('no')->references('no')->on('main');
        });
        
        Schema::table('mobile', function (Blueprint $table) {
            $table->foreign('no')->references('no')->on('main');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

$(document).ready(function(){
    $('#studi').on('change', function() {
        if ( this.value == '4' || this.value == '5' || this.value == '6')
        {
            $("#bidangstudi").show();
            $("#fakultas").hide();
        }
        else if(this.value == '7' || this.value == '8' || this.value == '9')
        {
            $("#bidangstudi").show();
            $("#fakultas").show();
        }
        else
        {
            $("#bidangstudi").hide();
            $("#fakultas").hide();
        }
    });
});
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Main extends Model
{
//    protected $fillable = ["report"];
    protected $table = 'main';
    public $timestamps = false;
    protected $primaryKey = 'no';
    protected $fillable = ['nama', 'perusahaan', 'nik', 'npwp', 'alamat', 'tempatlahir', 'tanggallahir', 'telponrumah', 'status', 'statuskaryawan', 'jabatan', 'tanggalmasuk', 'proyek', 'masakerja', 'agama', 'gender', 'golongandarah'];

}

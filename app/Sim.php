<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sim extends Model
{
    protected $table = 'sim';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = ['no', 'jenis', 'nomorsim'];
}

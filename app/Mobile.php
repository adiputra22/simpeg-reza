<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mobile extends Model
{
    protected $table = 'mobile';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = ['no', 'nomor'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perusahaan extends Model
{
    protected $table = 'perusahaan';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = ['nama'];

}

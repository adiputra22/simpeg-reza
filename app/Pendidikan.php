<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
    protected $table = 'pendidikan';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = ['no', 'jenjang', 'nama', 'periode', 'bidangstudi', 'fakultas'];

}

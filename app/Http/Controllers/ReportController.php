<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Report;
use App\User;
use Session;
use Auth;

class ReportController extends Controller
{
  public function getData()
 {
       if(Auth::check())
       {
           $user_id = Auth::user()->id;
           $reports = Report::where('userid', '=', $user_id)->where('created_at', '>=', Carbon::today())->get();

           return view('home', compact('reports'));
       }
       else
       {
           return view('home');
       }
 }

    public function store(Request $request)
    {
        $user_id = Auth::user()->id;
        $data = new Report;

        $data->report = $request->input('report');
        $data->userid = $user_id;
        $data->save();
        Session::flush();
        return redirect()->back();
    }
}

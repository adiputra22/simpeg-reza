<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Main;
use App\Perusahaan;
use App\Sim;
use App\Statuspgw;
use App\Kerja;
use App\Kursus;
use App\Pendidikan;
use App\Studi;

class ListController extends Controller
{
  public function getData()
	{
        $mains = Main::all();

        if(env('adminlte') == 'true') {
          return view('adminlte.index', compact('mains'));
        } else {
          return view('index', compact('mains'));
        }
	}

  public function getDataPerusahaan()
  {
    $perusahaans = Perusahaan::all();
    return view('show', compact('perusahaans'));
  }
}

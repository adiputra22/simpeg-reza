<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::auth();

Route::get('/', 'HomeController@index');
Route::post('/create', 'HomeController@store');
Route::get('/view', 'ListController@getData');
Route::get('/show', 'ListController@getDataPerusahaan');
Route::post('/showlist', 'HomeController@show');
Route::post('/edit', 'HomeController@edit');
Route::post('/update', 'HomeController@update');
Route::get('/delete/{id}', 'HomeController@delete');
Route::get('/perusahaan', 'PerusahaanController@index');
Route::get('/perusahaan/filter', 'PerusahaanController@filter');
Route::get('/perusahaan/download_pdf', 'PerusahaanController@download_pdf');
